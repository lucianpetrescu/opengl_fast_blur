## Linearly Sampled GPU Gaussian Blur ##

![opaque](img/out0.png)

This project implements a separable Gaussian Blur with linear sampling on the GPU, with OpenGL 4.5. Compared to the standard separable Gaussian blur the linearly sampled variant uses cheap GPU bilinear texture filtering to combine samples from the Gaussian kernel, halving the number of required samples, while producing the same result.

SPEED (measured on a NVIDIA GTX 960M laptop card):  
A 301 sized kernel with a sigma of 50 runs in 17.3 ms for a 1600x1200 image.  
A 17 sized kernel with a sigma of 5 runs in 5.14ms for a 960x640 image
A 7 sized kernel with a sigma of 2 runs in 2.11ms for a 512x512 image.

NOTE: OpenGL 4.5 compatible hardware is required to run this project.
 
#### HOW TO USE IN OTHER PROJECTS? ####
Include the files, and modify the next line: 

	// module needs OpenGL and wic provides it
	#include "../dependencies/lap_wic/lap_wic.hpp"

with a header which includes OpenGL functions. Then it's as simple as this:

	GPUGaussianBlur gaussian_blur;
	gaussian_blur.setInput(OPENGL_TEXTURE_ID);
	gaussian_blur.compute(GAUSSIAN_SIGMA, KERNEL_SIZE);
	OPENGL_BLURRED_TEXTURE_ID = gaussian_blur.getOutputGL();

#### CONTROLS ####

The application does not have a GUI in order to minimize code. The application is controlled through the keyboard. Here is a screenshot of the console with the output for help (H key):

![help](img/help.png)

### ALL SCREENSHOTS ###

Original image
![original](img/image.png)  
Sigma 1  
![original](img/sigma1.png)  
Sigma 2  
![original](img/sigma2.png)  
Sigma 5  
![original](img/sigma5.png)  
Sigma 10  
![original](img/sigma10.png)  
Sigma 20  
![original](img/sigma20.png)  
Sigma 30  
![original](img/sigma30.png)  
Sigma 40  
![original](img/sigma40.png)  
Sigma 50  
![original](img/sigma50.png)  


#### BUILDING and DEPENDENCIES ####

This project uses [lap_wic](https://bitbucket.org/lucianpetrescu/public) for all OpenGL context, input handling and windowing needs and [glm](http://glm.g-truc.net/0.9.8/index.html) for mathematics. 
The project should compile with any CPP11 compiler. A 2015 visual studio project is prodived in the /vstudio folder. makefiles for visual studio and make+GCC can be found in the root folder. This project requires an OpenGL 4.5 graphics card with up-to-date drivers 

Tested under Windows (vstudio and gcc with mingw). Should work under Unix / MacOS. 

#### LICENSE ####

The MIT License (MIT)
 
Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
 
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.