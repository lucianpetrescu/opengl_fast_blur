///------------------------------------------------------------------------------------------------------------------------------
/// The MIT License (MIT)
/// 
/// Copyright (c) 2017 Lucian Petrescu, lucian (dot) petrescu (dot) 24 (at) gmail (dot) com
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation 
/// files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, 
/// modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
/// is furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
/// OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE 
/// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR 
/// IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
///------------------------------------------------------------------------------------------------------------------------------


#include "gpu_fast_blur.h"

namespace blur {
	GPUGaussianBlur::GPUGaussianBlur() {
		//samplers
		sampler_near.setClamp();
		sampler_near.setNearest();
		sampler_bilinear.setClamp();
		sampler_bilinear.setBilinear();

		//gaussian blurring
		shader_blur.load("../shaders/linear_gaussian_blur.comp");
		gpu_tex1.create(GL_RGBA8, 10, 10, false);
		gpu_tex2.create(GL_RGBA8, 10, 10, false);
	}
	GPUGaussianBlur::~GPUGaussianBlur() {
	}

	void GPUGaussianBlur::setInput(GLuint texid) {
		int w, h;
		glGetTextureLevelParameteriv(texid, 0, GL_TEXTURE_WIDTH, &w);
		glGetTextureLevelParameteriv(texid, 0, GL_TEXTURE_HEIGHT, &h);
		input.width = w;
		input.height = h;
		input.texid = texid;
	}
	void GPUGaussianBlur::setInput(GPUTexture2D* in_input) {
		//input
		input.width = in_input->getWidth();
		input.height = in_input->getHeight();
		input.texid = in_input->getGLobject();

		//resize?
		if (input.width != gpu_tex1.getWidth() || input.height != gpu_tex1.getHeight()) {
			gpu_tex1.create(GL_RGBA8, input.width, input.height, false);
			gpu_tex2.create(GL_RGBA8, input.width, input.height, false);
		}
	}
	void GPUGaussianBlur::compute(float blur_kernel_sigma, unsigned int blur_kernel_size){
		unsigned int w = input.width;
		unsigned int h = input.height;
		
		//passthrough
		glBindTextureUnit(0, input.texid);
		sampler_near.bind(0);
		shader_blur.bind();
		shader_blur.setUniform("PASS", 0);
		gpu_tex1.bindAsImage(0, GL_WRITE_ONLY, 0, GL_RGBA8);
		glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
		glMemoryBarrier(GL_ALL_BARRIER_BITS);

		if (blur_kernel_size > 1) {
			//blur
			sampler_bilinear.bind(0);
			shader_blur.setUniform("SIGMA", blur_kernel_sigma);
			shader_blur.setUniform("SIZE", (int)(blur_kernel_size / 2));

			shader_blur.setUniform("PASS", 1);
			gpu_tex1.bindAsTexture(0);
			gpu_tex2.bindAsImage(0, GL_WRITE_ONLY, 0, GL_RGBA8);
			glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);

			shader_blur.setUniform("PASS", 2);
			gpu_tex2.bindAsTexture(0);
			gpu_tex1.bindAsImage(0, GL_WRITE_ONLY, 0, GL_RGBA8);
			glDispatchCompute(int(w + 31) / 32, int(h + 31) / 32, 1);
			glMemoryBarrier(GL_ALL_BARRIER_BITS);
		}
	}
	GPUTexture2D* GPUGaussianBlur::getOutput() {
		return &gpu_tex1;
	}
	GLuint GPUGaussianBlur::getOutputGL() {
		return gpu_tex1.getGLobject();
	}
	void GPUGaussianBlur::debugReloadShaders() {
		shader_blur.reload();
	}

}
