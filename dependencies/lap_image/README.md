# README #

LAP Image (LAPimg) is a tiny image loading library, written over stb_image image loading library
([https://github.com/nothings/stb revision 2.10]). It is written in a terse style, in order to minimize any integration effort and it is primarily useful for industries where problematic image formats can be avoided (education, game development, embedded).
LAPimg distinguishes itself from other tiny image libraries by supporting multi frame image operations, saving and loading of animated gifs (no transparency), per-channel and per-frame operations, various types of input operations and an extensive doxygen based documentation.

version 1.03

###PROPERTIES###
- c++11
- tested with vs2015 and gcc
- plug and play, just add lap\_image.hpp, lap\_image.cpp and lap\_image_dep.hpp to your build system
- supported input formats
	- JPEG baseline & progressive (12 bpc / arithmetic not supported, same as stock IJG lib)
	- PNG 1 / 2 / 4 / 8 - bit - per - channel (16 bpc not supported)
	- TGA
	- BMP non - 1bpp, non - RLE
	- PSD (composited view only, no extra channels, 8 / 16 bit - per - channel)
	- GIF 
	- animated GIF 
	- HDR (radiance rgbE format), the images are automatically converted to LDR with a gamma of 2.2 and a scale factor of 1.0. These can be changed per instance through the Image interface
	- PIC (Softimage PIC)
	- PNM (PPM and PGM binary only)
- supported output formats (when not specified done through stb)
	- BMP
	- GIF
	- PNG
	- animated GIF, alpha not supported (through jo_gif slightly modified)
- supports channel level operations (invert, set, switch, RGB->GBR, etc)
- support channel control on creation
- SRGB
	- support, on by default (as most color images are already SRGB)
	- can be specified through a ColorSpace parameter
	- SRGB (default) is non-linear and it is standard in photos, cameras and monitors, and it is used for color/albedo/diffuse/emission maps in rendering, https://en.wikipedia.org/wiki/SRGB
	- RGB is linear, and it is used for normal/bump/specular/glow/dudv/height/ao/noise maps , https://en.wikipedia.org/wiki/RGB\_color\_space
- comes with access operators
	- array style access with operator(unsigned int)
	- matrix/image style access with operator()(unsigned int x, unsigned int y, unsigned int channel)
	- multi-frame matrix/image style access with operator()(unsigned int x, unsigned int y, unsigned int channel, unsigned int frame)
	- access functions are also provided
- supported correct image resizing for both SRGB and RGB (through stb)
- no cpp exceptions (while more elegant, exceptions are not available/desired in/on some environments/platforms)

### COMPILING ###

Visual studio 2015 project&solution files can be found in the **vstudio** folder. Makefiles for gnu/gcc ( **makefile** ) and for visual studio ( **makefilevs.bat** ) are provided in the root folder. All methods will generate objs, pdbs, exes, etc in **bin**.

NOTE: segmentation fault produced with the MSYS 2 version of MinGW64, compiler bug.


### EXAMPLES ###

- Basic usage Usage:

		#include "lap_image.hpp"
		using namespace lap;
		lap::Image img(std::string("myfilename.ext"));
		img.transformGrayscale();
		img.save(std::string("mygrayscale.ext"));

- Channel ops basics:

		lap::Image img2(std::string("myfilename.ext"), Image::Channel::BLUE, Image::Channel::RED, image::Channel::GREEN);
		img2.save(std::string("mybrgfile.ext"));
		img2.transformRGBtoGRB();
		img2.save(std::string("myrbgfile.ext"));
		img2.channelSwitch(Image::Channel::GREEN, Image::Channel::BLUE);
		img2.save(std::string("myfilenamecopy.ext"));
		img2.saveChannel(std::string("myfilenameredchannel.ext"), Image::Channel::RED);

- Multiframe image basics:

		lap::Image img3(std::string("myanimated.gif"));
		img3.reverseFrames();
		img3.invert(Image::Channel::ALLNOA);
		img3.save(std::string("myreversedcolorinvertedanimated.gif"));
		img3.saveFrameChannel(std::string("myreversedcolorinvertedanimated_channelred_frame22.ext"), Image::Channel::RED, 22);


### DOCUMENTATION ###

a Doxyfile is provided in **doc**, which can be used with Doxygen to generate  the entire documentation in HTML format. 

### DEPENDENCIES ###

This project depends on:

- [stb\_image](https://github.com/nothings/stb/blob/master/stb_image.h)
- [stb\\_image\_resize](https://github.com/nothings/stb/blob/master/stb_image_resize.h)
- [stb\\_image\_write](https://github.com/nothings/stb/blob/master/stb_image_write.h)
- [jo_gif](http://www.jonolick.com/home/gif-writer)

### COMPATIBILITY ###

tested with visual studio 2015, MinGW x64 4.0, Ubuntu 15 + gcc 4.8.4, MacOS El Capitan + clang 3.7.1.
